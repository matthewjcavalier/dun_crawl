"""Setup file for the Project."""

from setuptools import setup

setup(name='dun_crawl',
      version='0.0.1',
      description='A rouge-like dungeon crawling game',
      author='Matt Cavalier',
      author_email='matt@cavalier.dev',
      license='MIT',
      packages=['dun_crawl'],
      # install_requires=[],
      test_suite='nose.collector',
      tests_require=['nose'],
      zip_safe=False)
