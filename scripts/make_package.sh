#!/bin/bash

echo "Hello World!"

read -p 'Package Name: ' packName
read -p 'Location: ' packPath

cd $packPath
mkdir $packName

cd $packName
touch __init__.py
touch ${packName}.py
mkdir test
cd test
touch __init__.py
touch test_${packName}.py
printf '"""Tests for %s package."""\n\nimport unittest\n\n\nclass Test_%s(unittest.TestCase):\n\n    def test_simple(self):\n        self.assertEqual(1, 1)' ${packName} ${packName} >> test_${packName}.py
echo "Created new Package ${packName} at location ${packPath}"

