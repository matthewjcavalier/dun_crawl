init:
	python3 setup.py develop

test:
	python3 setup.py test

package:
	./scripts/make_package.sh
