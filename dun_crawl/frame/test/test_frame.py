"""Tests for frame package."""

import unittest
from dun_crawl.frame import Frame
from dun_crawl.coordinate import Coord


class Test_frame(unittest.TestCase):
    def setUp(self):
        self.sut = Frame(3, 4, lambda: 5)

    def test_constructor_and_getters(self):
        self.assertEqual(3, self.sut.get_height())
        self.assertEqual(4, self.sut.get_width())
        for row in range(0, 3 - 1):
            for col in range(0, 4 - 1):
                self.assertEqual(5, self.sut.get(Coord(row, col)))

    def test_get(self):
        self.assertEqual(5, self.sut.get(Coord(0, 0)))
        got = self.sut.get(Coord(0, 0))
        got = 8
        self.assertEqual(5, self.sut.get(Coord(0, 0)))

    def test_set(self):
        self.sut.set(Coord(0, 0), 10)
        self.assertEqual(10, self.sut.get(Coord(0, 0)))
