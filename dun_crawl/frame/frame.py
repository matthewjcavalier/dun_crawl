"""Module involving data frames."""
from dun_crawl.coordinate import Coord


class Frame:
    """A 2D array holder that stores data."""

    def __init__(self, height: int, width: int, populater_fun):
        """Constructor."""
        self._data = [[0] * width] * height
        self._height = height
        self._width = width

        for row in range(0, height):
            for col in range(0, width):
                self._data[row][col] = populater_fun()

    def get_height(self) -> int:
        """Get the height of the internal array."""
        return self._height

    def get_width(self) -> int:
        """Get the width of the internal array."""
        return self._width

    def get(self, coord: Coord):
        """Get a copy of the data stored in this object at the coord."""
        if self._coord_is_valid(coord):
            return self._data[coord.y][coord.x]
        else:
            self._thorw_index_exception(coord)

    def set(self, coord: Coord, data):
        """Set the data stored in this object at the coord."""
        if self._coord_is_valid(coord):
            self._data[coord.y][coord.x] = data
        else:
            self._thorw_index_exception(coord)

    def _coord_is_valid(self, coord: Coord) -> bool:
        return (coord.x >= 0 and coord.x < self.get_width()
                and coord.y >= 0 and coord.y < self.get_height())

    def _thorw_index_exception(self, coord):
        raise Exception(f'index out of bounds, '
                        f'passed coord: {coord.to_string()} when max '
                        f'height: {self.get_height()} '
                        f'and max width: {self.get_width()}')
