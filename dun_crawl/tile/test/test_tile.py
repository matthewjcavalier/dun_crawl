"""Tests for tile package."""

import unittest
from dun_crawl.tile import TileType, Tile


class Test_tile(unittest.TestCase):

    def setUp(self):
        self.sut = Tile(1, TileType.WALL)

    def test_constructor(self):
        self.assertEqual(1, self.sut.get_hardness())
        self.assertEqual(TileType.WALL, self.sut.get_type())

    def test_set_harndess(self):
        self.sut.set_hardness(2)
        self.assertEqual(2, self.sut.get_hardness())

    def test_set_type_room(self):
        self.sut.set_type(TileType.ROOM)
        self.assertEqual(TileType.ROOM, self.sut.get_type())
        self.assertEqual(0, self.sut.get_hardness())

    def test_set_type_hall(self):
        self.sut.set_type(TileType.HALL)
        self.assertEqual(TileType.HALL, self.sut.get_type())
        self.assertEqual(0, self.sut.get_hardness())

    def test_set_type_wall_default_val(self):
        self.sut.set_type(TileType.WALL)
        self.assertEqual(TileType.WALL, self.sut.get_type())
        self.assertEqual(1, self.sut.get_hardness())

    def test_set_type_wall(self):
        self.sut.set_type(TileType.WALL, 10)
        self.assertEqual(TileType.WALL, self.sut.get_type())
        self.assertEqual(10, self.sut.get_hardness())




if __name__ == '__main__':
    unittest.main() 
