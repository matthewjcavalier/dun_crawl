"""Tile Module."""

from enum import Enum


class TileType(Enum):
    """Enumeration of tile types."""

    WALL = 1
    HALL = 2
    ROOM = 3



class Tile:
    """Tile that can be placed in a dungeon."""

    def __init__(self, hardness: int, type: TileType):
        """Class Constructor."""
        self._hardness = hardness
        self._type = type

    def get_hardness(self) -> int:
        return self._hardness

    def get_type(self) -> TileType:
        return self._type

    def set_hardness(self, hardness: int):
        self._hardness = hardness

    def set_type(self, type: TileType, hardness: int = 1):
        """
        Update the tile type and updates the hardness value depending on the
        incoming type.
        """
        self._type = type
        if type != TileType.WALL:
            self._hardness = 0
        else:
            self._hardness = hardness

