"""Settings object."""

import configparser


class Settings:
    """Object that holds game settings."""

    def __init__(self, config_str: str):
        """Constructor."""
        self._config = configparser.ConfigParser(allow_no_value=True)
        self._config.read_string(config_str)

    def get_dun_int_setting(self, setting: str):
        """Get the provided integer setting for the dungeon."""
        return self._config.getint('Dungeon', setting)

    def get_dun_string_setting(self, setting: str):
        """Get the provided integer setting for the dungeon."""
        return self._config.get('Dungeon', setting)

    def get_dun_bool_setting(self, setting: str):
        """Get the provided integer setting for the dungeon."""
        return self._config.getboolean('Dungeon', setting)
