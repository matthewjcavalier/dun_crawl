"""Tests for settings package."""

import unittest
from dun_crawl.settings import Settings


class Test_settings(unittest.TestCase):
    def setUp(self):
        self.sut = Settings("""
        [Dungeon]
        integer1 = 1
        string1 = one
        boolean1 = true
        null_1
        """)

    def test_get_dun_setting(self):
        self.assertEqual(1, self.sut.get_dun_int_setting('integer1'))
        self.assertEqual('one', self.sut.get_dun_string_setting('string1'))
        self.assertTrue(self.sut.get_dun_bool_setting('boolean1'))
        self.assertIsNone(self.sut.get_dun_string_setting('null_1'))
