"""Class for rooms."""
from dun_crawl.coordinate import Coord


class Room:
    """Room object."""

    def __init__(self, loc: Coord, height: int, width: int):
        """Constructor."""
        self._loc = loc
        self._height = height
        self._width = width

    def get_height(self) -> int:
        return self._height

    def get_width(self) -> int:
        return self._width

    def get_top_left(self) -> Coord:
        """Get the coord of the top left corner of the room."""
        return self._loc

    def get_top_right(self) -> Coord:
        """Get the coord of the top right corner of the room."""
        return Coord(self._loc.get_x() + self.get_width(), self._loc.get_y())

    def get_bottom_right(self) -> Coord:
        """Get the coord of the bottom right corner of the room."""
        return Coord(self._loc.get_x() + self.get_width(), self._loc.get_y() + self.get_height())

    def get_bottom_left(self) -> Coord:
        """Get the coord of the bottom left corner of the room."""
        return Coord(self._loc.get_x(), self._loc.get_y() + self.get_height())


def _stringify(row: int, col: int) -> str:
    return f"x:{col},y:{row}"


def rooms_overlap(r1: Room, r2: Room, buffer: int = 1) -> bool:
    """
        Check if two rooms occupy the same space while accounting for a buffer
        r1: Room
        r2: Room
        buffer: int
            This is the number of tiles that must be separating the two rooms,
            defaults to 2
    """
    r1_point_set = set()
    for col in range(r1.get_top_left().get_x() - buffer,
                     r1.get_top_right().get_x() + buffer):
        for row in range(r1.get_top_left().get_y() - buffer,
                         r1.get_bottom_left().get_y() + buffer):
            r1_point_set.add(_stringify(row, col))

    for col in range(r2.get_top_left().get_x(), r2.get_top_right().get_x()):
        for row in range(r2.get_top_left().get_y(),
                         r2.get_bottom_left().get_y()):
            if _stringify(row, col) in r1_point_set:
                return True
    return False
