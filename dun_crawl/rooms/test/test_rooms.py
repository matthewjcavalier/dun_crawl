"""Tests for rooms package."""

import unittest
from dun_crawl.rooms import Room
from dun_crawl.rooms import rooms_overlap

from dun_crawl.coordinate import Coord


class Test_rooms(unittest.TestCase):
    
    def setUp(self):
        self.sut = Room(Coord(1,1), 5, 6)

    def test_simple(self):
        self.assertEqual(1, 1)

    def test_constructor(self):
        self.assertEqual(5, self.sut.get_height())
        self.assertEqual(6, self.sut.get_width())
        self.assertEqual(Coord(1, 1), self.sut.get_top_left())
        self.assertEqual(Coord(7, 1), self.sut.get_top_right())
        self.assertEqual(Coord(7, 6), self.sut.get_bottom_right())
        self.assertEqual(Coord(1, 6), self.sut.get_bottom_left())

    def test_rooms_overlap_defualt_no_overlap(self):
        r1 = Room(Coord(0,0), 2, 2)
        r2 = Room(Coord(3,3), 2, 2)
        self.assertFalse(rooms_overlap(r1, r2))

    def test_rooms_overlap_buffer_2_overlap(self):
        r1 = Room(Coord(0,0), 2, 2)
        r2 = Room(Coord(3,3), 2, 2)
        self.assertTrue(rooms_overlap(r1, r2, 2))
