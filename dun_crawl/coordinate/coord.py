"""A Coordinate."""


class Coord:
    """Thing."""

    def __init__(self, x, y):
        """Constructor."""
        self.x = x
        self.y = y

    def get_x(self):
        """Return the internal value of x."""
        return self.x

    def get_y(self):
        """Return the internal value of y."""
        return self.y

    def __eq__(self, other):
        """Check to see if the two coords are the same point."""
        return self.get_x() == other.get_x() and self.get_y() == other.get_y()

    def to_string(self) -> str:
        """Generate a string version of this coord."""
        return f'x:{self.get_x()},y:{self.get_y()}'


def coord_above(coord):
    """Make a new Coord object that is above the input one."""
    return Coord(coord.get_x(), coord.get_y() - 1)


def coord_below(coord):
    """Make a new Coord object that is below the input one."""
    return Coord(coord.get_x(), coord.get_y() + 1)


def coord_right(coord):
    """Make a new Coord object that is to the right of the input one."""
    return Coord(coord.get_x() + 1, coord.get_y())


def coord_left(coord):
    """Make a new Coord object that is to the left of the input one."""
    return Coord(coord.get_x() - 1, coord.get_y())
