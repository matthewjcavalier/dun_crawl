"""Unit tests for the coord module."""


import unittest
from dun_crawl.coordinate import Coord
from dun_crawl.coordinate import coord_above, coord_below
from dun_crawl.coordinate import coord_left, coord_right


class TestCoord(unittest.TestCase):

    def setUp(self):
        self.sut = Coord(1, 1)

    def test_init(self):
        self.assertEqual(1, self.sut.get_x())
        self.assertEqual(1, self.sut.get_y())

    def test_eq_true(self):
        sut_2 = Coord(1, 1)
        self.assertTrue(self.sut == sut_2)
        self.assertTrue(sut_2 == self.sut)

    def test_eq_false(self):
        sut_2 = Coord(2, 2)
        self.assertFalse(self.sut == sut_2)
        self.assertFalse(sut_2 == self.sut)

    def test_above(self):
        expected = Coord(1, 0)
        actual = coord_above(self.sut)
        self.assertTrue(expected == actual)

    def test_below(self):
        expected = Coord(1, 2)
        actual = coord_below(self.sut)
        self.assertTrue(expected == actual)

    def test_left(self):
        expected = Coord(0, 1)
        actual = coord_left(self.sut)
        self.assertTrue(expected == actual)

    def test_right(self):
        expected = Coord(2, 1)
        actual = coord_right(self.sut)
        self.assertTrue(expected == actual)

    def test_to_string(self):
        expected = "x:1,y:1"
        actual = self.sut.to_string()
        self.assertEqual(expected, actual)
