"""Main Game."""

from dun_crawl.settings import Settings

default_settings_loc = "../resources/game.ini"
file_contents = ''

with open(default_settings_loc) as file:
    file_contents = file.read()

game_settings = Settings(file_contents)

print(game_settings.get_dun_int_setting('seed'))
